**SakuraKo Project**

SakuraKo propose une solution d'intégration continue et de mise en production automatique de projets Spring Boot déployés sur serveur Tomcat. 

**Pré-requis logiciels**

VirtualBox et Vagrant
[vérifier que les versions soient compatibles ou utiliser les versions de développement par défaut (cf.*Développé avec*)]
Git

**Installation de l'infrastructure**

Sur l'OS du poste de travail, créer un dossier "SakuraKo".
Dans le dossier, exécuter une commande ***git clone https://gitlab.com/edegouys/projetcicd/***.

SakuraKo Project embarque quatre machines virtuelles Vagrant dotées de 3Gb de RAM sur base d'un Centos7.

La machine Master, comme son nom l'indique, est la machine pivot de l'infrastructure du projet. 
Entièrement provisionnée par son Vagrantfile, elle embarque un JDK8, Jenkins, Ansible et Git.
Master permet, entre autres, de lancer des rôles Ansible afin de provisionner les autres machines de l'infrastructure et de lancer Jenkins sur le port local 8080.

Les machines Slave, Tests et Prod sont provisionnés par Master via des rôles Ansible : JDK8, GIT & DOCKER.
La machine Slave intègre, en plus, une installation de Maven dans son vagrantfile (car elle sera la seule machine à l'utiliser).

Afin de mettre en oeuvre cette infra, il s'agit de :

**1. Lancer les machines**
Pour lancer les machines, exécuter successivement les commandes ***Vagrant Up*** puis ***Vagrant ssh*** dans les répertoires contenant les vagrantfiles de chacune des quatre machines.

**2. Etablir les connexions SSH entre machines**
- Depuis Master, générer une paire de clefs SSH grâce à la commande ***ssh-keygen***;
- A partir du chemin ***/home/vagrant/.ssh*** de master, récupérer la clef publique en lançant la commande ***cat id_rsa.pub***;
- Dans les répertoires ***/home/vagrant/.ssh*** de Slave, Prod et Test, ajouter la clef publique de master dans le fichier ***authorized_keys*** en éxécutant la commande ***sudo vi authorized_keys***;
- Effectuer les même opérations en générant une paire de clefs à partir de Slave pour les machines Prod et Test.
- Vérifier les connexions ssh depuis Master et depuis Slave [***sudo ssh vagrant@IP -p port-visé***] afin de créer les fichiers ***Known_hosts***(au besoin, vérifier que le service sshd est bien actif)
Pour informations, les ports SSH sont les suivants :
 - Slave : 9100
 - Test : 9200
 - Prod : 9300
**PS: l'IP utilisée dans les différents inventaires est l'IP du poste de développement du projet. Il faudra penser à la changer dans chaque inventory Ansible.**

**3. Provisionner les machines via Ansible**
Depuis master :
- Import du repo de projet : ***git clone https://gitlab.com/edegouys/projetcicd/***
- Dans ***projetcicd/repo_ansible***, exécuter la commande ***sudo ansible-playbook -i inventory installer.yml***
- Vérifier sur les autres machines si l'installation a été correctement effectuée en exécutant les commandes:
***git --version***
***java -version***
***docker -v***
    
**Démarrage**

Sur un navigateur internet, aller sur l'url ***http://localhost:8080*** (Master doit être allumée. Si Jenkins ne se lance pas, vérifier le status du service).
- Paramétrer l'utilisateur administrateur;
- Installer les plugins Docker et Ansible;
- Créer deux identifiants :
    - Un credential SSH avec la clef privée de Master.
    - Un credential Login/Password Dockerhub
- Créer les 3 noeuds Slave / Test / Prod en utilisant le credential SSH et les lancer.
- Créer un job pipeline :
- Dans Pipeline, choisir dans la liste déroulante l'option ***Pipeline script from SCM***;
- Choisir GIT comme SCM;
- Entrer l'url du repo de projet : ***https://gitlab.com/edegouys/projetcicd/*** en restant sur la branche Master.
        
***Il est possible de checker si commits sur Master et de lancer des builds le cas échéant pour remplacer l'option de Webhook qui n'a pas pu être mise en oeuvre dans le cadre de ce projet.***
Dans le job, dans les Builds Triggers, option construire périodiquement : H/1 * * * * (Check toutes les minutes)

Lancer un build et admirer le résultat...

Les images dockers buildées, contenant l'application et l'environnement d'éxécution, sont disponibles à l'adresse suivante : 
***https://hub.docker.com/r/haithemboukhari/image_master/tags*** pour les releases;
***https://hub.docker.com/r/haithemboukhari/image_develop/tags*** pour les snapshots develop.

**Rollback**

Un job Jenkins de Rollback est disponible dans le repo du projet: Jenkinsfile_Rollback.
Il est possible, en éxécutant ce job, de choisir en paramètres [en cochant l'option ***Ce build a des paramètres / paramètre String / Nom : BUILD_ID***] :
Le numéro de version à builder (cf. Numéro de la version en tag de l'image docker)

**Développé avec**
    
- Windows 10 Pro - OS
- Virtual Box 6.0.14 et Vagrant 2.2.6 - Machines virtuelles
- Visual Studio Code - Editeur
- Trello - Gestion de projet : https://trello.com/b/afInPEda/projet-cicd
- GitLab - Source Controle Manager: 
    - Repo du projet : https://gitlab.com/edegouys/projetcicd/
    - Repo App Test : https://gitlab.com/RaphaeldeGail/devopsapp/


**Auteurs**

Haithem Boukhari @haithem-boukhari - haithemboukhari@gmail.com

Estelle Degouys @edegouys - estelle.degouys@gmail.com